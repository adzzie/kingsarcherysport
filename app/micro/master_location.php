<?php
$app = getMicroApp();
global $request;
$request = getRequestApp();


$app->get('/api/get_regency', function () {
	global $request;
	$id = $request->getQuery('id');
	$ids = $request->getQuery('ids');
	$id_province = $request->getQuery('id_province');
	$id_provinces = $request->getQuery('id_provinces');

	$conditions = "1=1 ";
	$bind = array();
	if ( isset($id) ) {
		$conditions .= " and id = :id:";
		$bind[ 'id' ] = $id;
	}
	if ( isset($ids) ) {
		$conditions .= " and id in (" . $ids . ")";
		$bind[ 'ids' ] = $ids;
	}
	if ( isset($id_province) ) {
		$conditions .= " and id_province = :id_province:";
		$bind[ 'id_province' ] = $id_province;
	}
	if ( isset($id_provinces) ) {
		$conditions .= " and id_province in (" . $id_provinces . ")";
		$bind[ 'id_provinces' ] = $id_provinces;
	}
	echo json_encode(PgMstRegency::find(array(
		"conditions" => $conditions,
		"bind" => $bind
	))->toArray());
});

$app->get('/api/get_sub_district', function () {
	global $request;
	$id = $request->getQuery('id');
	$ids = $request->getQuery('ids');
	$id_regency = $request->getQuery('id_regency');
	$id_regencies = $request->getQuery('id_regencies');

	$conditions = "1=1 ";
	$bind = array();
	if ( isset($id) ) {
		$conditions .= " and id = :id:";
		$bind[ 'id' ] = $id;
	}
	if ( isset($ids) ) {
		$conditions .= " and id in (" . $ids . ")";
		$bind[ 'ids' ] = $ids;
	}
	if ( isset($id_regency) ) {
		$conditions .= " and id_regency = :id_regency:";
		$bind[ 'id_regency' ] = $id_regency;
	}
	if ( isset($id_regencies) ) {
		$conditions .= " and id_regency in (" . $id_regencies . ")";
		$bind[ 'id_regencies' ] = $id_regencies;
	}
	echo json_encode(PgMstSubDistrict::find(array(
		"conditions" => $conditions,
		"bind" => $bind
	))->toArray());
});

$app->get('/api/get_current_location', function () {
	$result = CommonLibrary::getRequestGeoInfo();
	echo json_encode($result);
});