<?php
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

$app = getMicroApp();
global $request;
$request = getRequestApp();

/**
 * Created by PhpStorm.
 * User: Hendra
 * Date: 10/10/2015
 * Time: 11:35 PM
 */

$app->get('/api/news/notif/getAllNewsNotif', function () {
    global $request;
    header('Content-Type: application/json');

    $app_key = $request->getQuery('app_key');
    $session_key = $request->getQuery('session_key');
    //$idMember = $request->getQuery('id');
    $android_version = intval($request->getQuery("android_version"));
    $idMember = MemberService::getUserLoggedOnBySessionToken($session_key);

    $response = new ResponseObject();
    $response->isSuccess = FALSE;
    $response->message = "FAILED PROSES";

    if (MemberService::checkAppKey($app_key) === FALSE) {
        header('HTTP/1.0 401 Authorization Required');
        $newsDataHeader = array(
            "isSuccess" => FALSE,
            "message" => T::message("login.message.invalidappkey")
        );
        echo json_encode($newsDataHeader);
        die();
    }
    if (MemberService::checkSessionToken($session_key) === FALSE) {
        header('HTTP/1.0 401 Authorization Required');
        $businessDataHeader = array(
            "isSuccess" => FALSE,
            "message" => T::message("login.message.invalidsessionkey")
        );
        echo json_encode($businessDataHeader);
        die();
    }

    $modelsManager = CommonLibrary::getModelsManager();
    $newsBisnisImagesData = array();
    /*=================================================================*/
    $sql5 = "select c.created_time as tgl,
                    c.detail_title,
                    m.first_name, m.last_name,
                    bs.business_name,
                    a.type_data,
                    a.id
               from KdgNotification a
                join KdgNewsBusinessApproval as b on a.id_fk = b.id
                join KdgNewsBusiness as c on b.id_news = c.id
                join KdgMstBusiness as bs on bs.id = c.id_business
                join KdgMstMember as m on m.id = a.id_member
              where a.status_active = 1
                and a.type_data NOT IN (201, 24)
                and a.id_member=:id_member:";
    $listData5 = $modelsManager->executeQuery($sql5, ["id_member"=>$idMember]);
    foreach ($listData5 as $dataA) {
        $texts = BusinessService::getTexts($dataA);
        $newsBisnisImagesData[] = array(
            "notifikasiTgl" => $dataA->tgl,
            "notifikasiText" => $texts,
            "notifikasiType" => $dataA->type_data,
            "notifikasiTypeNotifDesc" => TypeNotification::getString($dataA->type_data),
            "notifikasiId" => $dataA->id
        );
    }

    $sql420 = "select
		            c.created_time as tgl,
		            c.detail_title,
		            a.type_data,
		            a.id
	           from KdgNotification as a
	           join KdgNewsBusiness as c on a.id_fk = c.id
              where a.status_active = 1
                and a.type_data NOT IN (201, 24)
                and id_member =:id_member:";
    $listData420 = $modelsManager->executeQuery($sql420, ["id_member"=>$idMember]);
    foreach ($listData420 as $dataA) {
        $texts = BusinessService::getTexts($dataA);
        $newsBisnisImagesData[] = array(
            "notifikasiTgl" => $dataA->tgl,
            "notifikasiText" => $texts,
            "notifikasiType" => $dataA->type_data,
            "notifikasiTypeNotifDesc" => TypeNotification::getString($dataA->type_data),
            "notifikasiId" => $dataA->id
        );
    }

    $sql12310 = "SELECT
                a.time_created AS tgl, a.id_member_creator,
                b.first_name, b.last_name,
                c.subtitle,c.divtitle,
                c.trade_business_name,
                a.type_data,
                d.job_role, d.notes,
                a.id
            FROM
                KdgNotification as a
                JOIN KdgMstMember as b ON b.id = a.id_member_creator
                JOIN KdgMstBusiness as c ON  c.id = a.id_business_creator
                JOIN KdgRelMemberBusiness as d ON d.id_member = a.id_member
            where a.status_active = 1
              and a.type_data NOT IN (201, 24)
              and a.id_fk = d.id_business
              and a.id_member =:id_member: ";
    $listData12310 = $modelsManager->executeQuery($sql12310, ["id_member"=>$idMember]);
    foreach ($listData12310 as $dataA) {
        //$dataB = KdgMstMember::findFirstById($dataA->id_member_creator);
        $texts = BusinessService::getTexts($dataA);
        $newsBisnisImagesData[] = array(
            "notifikasiTgl" => $dataA->tgl,
            "notifikasiText" => $texts,
            "notifikasiType" => $dataA->type_data,
            "notifikasiTypeNotifDesc" => TypeNotification::getString($dataA->type_data),
            "notifikasiId" => $dataA->id
        );
    }

    if( count($newsBisnisImagesData) > 0 ){
        /*$response->isSuccess = TRUE;
        $response->message = "Several data found.";
        $response->data = array("data"=>$newsBisnisImagesData,"pageCurrent"=>1);*/
        $resultData=array(
            "isSuccess"=>TRUE,
            "message"=>"Several data found",
            "data"=>$newsBisnisImagesData,
            "pageCurrent"=>1
        );

    }else{
        /* $response->isSuccess = TRUE;
         $response->message = "Data not found";
         $response->data = array("pageCurrent"=>1);*/

        $resultData=array(
            "isSuccess"=>TRUE,
            "message"=>"Data Not found",
            "data"=>array(),
            "pageCurrent"=>1
        );

    }

    echo json_encode($resultData);
});