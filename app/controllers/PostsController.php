<?php

use Phalcon\Mvc\Controller;

class PostsController extends Controller
{

    public function indexAction()
    {

    }

    public function initialize()
    {
        $this->view->setTemplateBefore('common');
    }

    public function lastAction()
    {
        $this->flash->notice("These are the latest posts");
    }

}

