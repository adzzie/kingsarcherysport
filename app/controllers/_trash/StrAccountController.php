<?php
namespace AdminModul;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class StrAccountController extends AdminControllerBase{

    private $MODEL = "StrAccount";
    private $TITLE = "Account";
    private $TITLE_BREAD = "List Account";
    private $URL = "str_account";

    public function indexAction(){

        $model = array();
        $model['title'] = $this->TITLE;
        $model['title_bread'] = $this->TITLE_BREAD;
        $model['url'] = $this->URL;
        $model['url_form'] = '/admin/' . $this->URL . '/new';

        StrAccountController::loadData($this->URL, $model);
    }

    public function loadData($url, $model){
        $dataArray=\StrAccount::find()->toArray();
        $this->view->partial('admin/' . $url . '/index',
            array("model"=>$model, "dataArray"=>json_decode(json_encode($dataArray)))
        );
    }

    public function newAction(){
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $model = array();
        $model['id'] = $id;

        $model['title'] = $this->TITLE;
        $model['title_bread'] = "Form";

        $model['url'] = $this->URL;
        $model['url_list'] = '/admin/' . $this->URL . '/index';

        $model['action'] = "save";
        $model['labelSubmit'] = "Simpan";

        $this->view->partial('admin/'.$this->URL.'/form', array("model"=>$model));
    }

    public function listAction(){

        $start = isset($_GET['start']) ? $_GET['start'] : 0;
        $length = isset($_GET['length']) ? $_GET['length'] : 10;
        $draw = isset($_GET['draw']) ? $_GET['draw'] : 0;
        $search = isset($_GET['search']) ? $_GET['search']['value'] : "";

        $conditions = "(username like :search:)
                            ORDER BY date_updated DESC";
        $bind = array("search" => "%" . $search . "%");

        $listData = \StrAccount::find(array(
            "conditions" => $conditions,
            "limit" => $length,
            "offset" => $start,
            "bind" => $bind
        ));


        $list = array();
        $idx = 0;
        foreach ($listData as $data) {
            $list[$idx] = $data->toArray();
            $list[$idx]["username"] = \Safe::make($data)->username->safe_get('');
            $idx++;
        }
        $count = \StrAccount::count(array(
            "conditions" => $conditions,
            "bind" => $bind
        ));
        $total = $count;

        $results = array(
            "draw" => $draw,
            "recordsTotal" => $total,
            "recordsFiltered" => $count,
            "data" => $list
        );
        echo json_encode($results);
    }

    /**
     * Searches for str_account
     */
    public function searchAction()
    {

        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "StrAccount", $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "id";

        $str_account = StrAccount::find($parameters);
        if (count($str_account) == 0) {
            $this->flash->notice("The search did not find any str_account");

            return $this->dispatcher->forward(array(
                "controller" => "str_account",
                "action" => "index"
            ));
        }

        $paginator = new Paginator(array(
            "data" => $str_account,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Edits a str_account
     *
     * @param string $id
     */
    public function editAction($id)
    {

        if (!$this->request->isPost()) {

            $str_account = StrAccount::findFirstByid($id);
            if (!$str_account) {
                $this->flash->error("str_account was not found");

                return $this->dispatcher->forward(array(
                    "controller" => "str_account",
                    "action" => "index"
                ));
            }

            $this->view->id = $str_account->id;

            $this->tag->setDefault("id", $str_account->id);
            $this->tag->setDefault("id_role", $str_account->id_role);
            $this->tag->setDefault("id_user", $str_account->id_user);
            $this->tag->setDefault("username", $str_account->username);
            $this->tag->setDefault("password", $str_account->password);
            $this->tag->setDefault("sts_aktif", $str_account->sts_aktif);
            $this->tag->setDefault("date_created", $str_account->date_created);
            $this->tag->setDefault("date_updated", $str_account->date_updated);
            
        }
    }

    /**
     * Creates a new str_account
     */
    public function createAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "str_account",
                "action" => "index"
            ));
        }

        $str_account = new StrAccount();

        $str_account->id = $this->request->getPost("id");
        $str_account->id_role = $this->request->getPost("id_role");
        $str_account->id_user = $this->request->getPost("id_user");
        $str_account->username = $this->request->getPost("username");
        $str_account->password = $this->request->getPost("password");
        $str_account->sts_aktif = $this->request->getPost("sts_aktif");
        $str_account->date_created = $this->request->getPost("date_created");
        $str_account->date_updated = $this->request->getPost("date_updated");
        

        if (!$str_account->save()) {
            foreach ($str_account->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "str_account",
                "action" => "new"
            ));
        }

        $this->flash->success("str_account was created successfully");

        return $this->dispatcher->forward(array(
            "controller" => "str_account",
            "action" => "index"
        ));

    }

    /**
     * Saves a str_account edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "str_account",
                "action" => "index"
            ));
        }

        $id = $this->request->getPost("id");

        $str_account = StrAccount::findFirstByid($id);
        if (!$str_account) {
            $this->flash->error("str_account does not exist " . $id);

            return $this->dispatcher->forward(array(
                "controller" => "str_account",
                "action" => "index"
            ));
        }

        $str_account->id = $this->request->getPost("id");
        $str_account->id_role = $this->request->getPost("id_role");
        $str_account->id_user = $this->request->getPost("id_user");
        $str_account->username = $this->request->getPost("username");
        $str_account->password = $this->request->getPost("password");
        $str_account->sts_aktif = $this->request->getPost("sts_aktif");
        $str_account->date_created = $this->request->getPost("date_created");
        $str_account->date_updated = $this->request->getPost("date_updated");
        

        if (!$str_account->save()) {

            foreach ($str_account->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "str_account",
                "action" => "edit",
                "params" => array($str_account->id)
            ));
        }

        $this->flash->success("str_account was updated successfully");

        return $this->dispatcher->forward(array(
            "controller" => "str_account",
            "action" => "index"
        ));

    }

    /**
     * Deletes a str_account
     *
     * @param string $id
     */
    public function deleteAction($id)
    {

        $str_account = StrAccount::findFirstByid($id);
        if (!$str_account) {
            $this->flash->error("str_account was not found");

            return $this->dispatcher->forward(array(
                "controller" => "str_account",
                "action" => "index"
            ));
        }

        if (!$str_account->delete()) {

            foreach ($str_account->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "str_account",
                "action" => "search"
            ));
        }

        $this->flash->success("str_account was deleted successfully");

        return $this->dispatcher->forward(array(
            "controller" => "str_account",
            "action" => "index"
        ));
    }

}
