<?php
namespace AdminModul;
use Phalcon\Http\Client\Exception;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class DashboardController extends \ControllerBase{

    private $TITLE = "Steril Yuk Panel";
    private $TITLE_BREAD = "Dashboard";
    private $URL = "dashboard";

    public function indexAction(){

        $model = array();
        $model['title'] = $this->TITLE;
        $model['title_bread'] = $this->TITLE_BREAD;
        $model['url'] = $this->URL;
        //$this->view->partial('admin/' . $this->URL . '/index', $model);
        DashboardController::loadData($this->URL, $model);
    }

    public function loadData($url, $model){
        $dataArray = array();
        $this->view->partial('admin/' . $url . '/index',
            array("model"=>$model, "dataArray"=>json_decode(json_encode($dataArray)))
        );
    }

    public function loadProdukAction(){
        $pageName = "Produk";
        $this->view->partial('admin/dashboard/produk',
            array("model"=>"", "pageName"=>$pageName)
        );
    }

    public function loadKegiatanAction(){
        $pageName = "Kegiatan";
        $this->view->partial('admin/dashboard/kegiatan',
            array("model"=>"", "pageName"=>$pageName)
        );
    }

    public function loadMoreAction(){
        $pageName = "More Product";
        $this->view->partial('admin/dashboard/ajax/loadMore',
            array("model"=>"", "pageName"=>$pageName)
        );
    }

    public function showDetailAction(){
        $pageName = "Detail Product";
        $imgProduct = $this->request->getQuery('img_product');
        $catProduct = $this->request->getQuery('cat_product');

        $this->view->partial('admin/dashboard/ajax/project1',
            array("model"=>"", "pageName"=>$pageName, "imgProduct"=>$imgProduct, "cat_product"=>$catProduct)
        );
    }

    public function showDetail2Action(){
        $pageName = "Detail Product";
        $imgProduct = $this->request->getQuery('img_product');
        $catProduct = $this->request->getQuery('cat_product');

        $this->view->partial('admin/dashboard/ajax/project2',
            array("model"=>"", "pageName"=>$pageName, "imgProduct"=>$imgProduct, "cat_product"=>$catProduct)
        );
    }

    public function showDetail3Action(){
        $pageName = "Detail Product";
        $this->view->partial('admin/dashboard/ajax/project3',
            array("model"=>"", "pageName"=>$pageName)
        );
    }

    public function showDetail4Action(){
        $pageName = "Detail Product";
        $this->view->partial('admin/dashboard/ajax/project4',
            array("model"=>"", "pageName"=>$pageName)
        );
    }
}