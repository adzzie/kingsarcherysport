<?php
namespace AdminModul;
use Phalcon\Http\Client\Exception;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class KegiatanController extends \ControllerBase{

    private $MODEL = "kategori";
    private $TITLE = "Kings Panel";
    private $TITLE_BREAD = "M. Kegiatan";
    private $URL = "kegiatan";

    public function indexAction(){
        $model = array();
        $model['title'] = $this->TITLE;
        $model['title_bread'] = $this->TITLE_BREAD;
        $model['url'] = $this->URL;
        //$this->view->partial('admin/' . $this->URL . '/index', $model);
        KegiatanController::loadData($this->URL, $model);
    }

    public function loadData($url, $model){
        $dataArray = array();
        $this->view->partial('admin/' . $url . '/index',
            array("model"=>$model, "dataArray"=>json_decode(json_encode($dataArray)))
        );
    }

    public function loadKegiatanAction(){
        $model = array();
        $model['pageName'] = "Kegiatan Master";
        $model['url'] = $this->URL;
        $this->view->partial('admin/kegiatan/index', $model);
    }

    public function listAction(){
        $start = isset($_GET['start'])?$_GET['start']:0;
        $length = isset($_GET['length'])?$_GET['length']:0;
        $draw = isset($_GET['draw'])?$_GET['draw']:0;
        $search = isset($_GET['search'])?$_GET['search']['value']:"";

        $conditions = "(username like :search:) ORDER BY date_updated DESC";
        $bind = array("search"=>"%".$search."%");

        $listData = \StrAccount::find(array(
            "conditions"=>$conditions,
            "limit"=>$length,
            "offset"=>$start,
            "bind"=>$bind
        ));
        $list = array();
        $idx = 0;
        foreach($listData as $data){
            $list[$idx] = $data->toArray();
            $list[$idx]["index"] = \Safe::make($idx)->safe_get('0');
            $list[$idx]["nama1"] = \Safe::make($data)->username->safe_get('');
            $list[$idx]["nama2"] = \Safe::make($data)->username->safe_get('');

            $idx++;
        }
        $count = \StrAccount::count(array(
            "conditions"=>$conditions,
            "bind"=>$bind
        ));
        $total = $count;

        $results = array(
            "draw" => $draw,
            "recordsTotal" => $total,
            "recordsFiltered" => $count,
            "data"=>$list
        );
        echo json_encode($results);
    }

    public function newAction(){
        $id = isset($_GET['id']) ? $_GET['id'] : "0";
        $model = array();
        $model['id'] = $id;
        $model['pageName'] = "Kegiatan Form";
        $model['url'] = $this->URL;
        $model['action'] = "save";
        $model['labelSubmit'] = "Save";
        $model['labelForm'] = "Create";

        $this->view->partial('admin/'.$this->URL.'/form', $model);
    }
}