<?php
use Phalcon\Logger\Adapter\File as FileAdapter;

class PartialLibrary
{
	public static function load( $path )
	{
        return base_url() . "/partial?path=" . McryptLibrary::encryptString($path);
	}

	public static function p_load( $path)
	{
        echo PartialLibrary::load($path);
	}
}