<?php
require __DIR__."/../../vendor/autoload.php";
use Phalcon\Logger\Adapter\File as FileAdapter;
use Abraham\TwitterOAuth\TwitterOAuth;

class TwitterLibrary
{

	public static function getConnection(){
		$consumerKey = PgVariable::findFirstById('TWITTER_CONSUMER_KEY');
		$consumer_key = $consumerKey->value;
		$consumerSecret = PgVariable::findFirstById('TWITTER_CONSUMER_SECRET');
		$consumer_secret = $consumerSecret->value;
		$accessToken = PgVariable::findFirstById('TWITTER_ACCESS_TOKEN');
		$access_token = $accessToken->value;
		$accessTokenSecret = PgVariable::findFirstById('TWITTER_ACCESS_TOKEN_SECRET');
		$access_token_secret=$accessTokenSecret->value;
		$connection = new TwitterOAuth($consumer_key, $consumer_secret, $access_token, $access_token_secret);
		return $connection;
	}

	public static function verify(){
		$connection = self::getConnection();
		$content = $connection->get("account/verify_credentials");
		return $content;
	}


	public static function getUserTimeline($opts = array()){

		$isStart = isset($opts['isStart']) ? $opts['isStart']:false;
		$since_id = isset($opts['since_id']) ? $opts['since_id']:false;
		$count = isset($opts['count']) ? $opts['count']:false;
		$max_id = isset($opts['max_id']) ? $opts['max_id']:false;
		$trim_user = isset($opts['trim_user']) ? $opts['trim_user']:false;
		$exclude_replies = isset($opts['exclude_replies']) ? $opts['exclude_replies']:false;
		$contributor_details = isset($opts['contributor_details']) ? $opts['contributor_details']:false;
		$include_rts = isset($opts['include_rts']) ? $opts['include_rts']:false;


		$connection = self::getConnection();
		$content = $connection->get("account/verify_credentials");

		$data = array();

//		$data['user_id'] = $content->id;
		$data['screen_name'] = $content->screen_name;
//		if($since_id){
//			$data['since_id'] = $since_id;
//		}
//		if($count){
//			$data['count'] = $count;
//		}
//		if($max_id){
//			$data['max_id'] = $max_id;
//		}
//		if($trim_user){
//			$data['trim_user'] = $trim_user;
//		}
//		if($exclude_replies){
//			$data['exclude_replies'] = $exclude_replies;
//		}
//		if($contributor_details){
//			$data['contributor_details'] = $contributor_details;
//		}
//		if($include_rts){
//			$data['include_rts'] = $include_rts;
//		}


		$data = array_merge($data,$opts);

//var_dump("================================== user time line=================================");
		$timelines = $connection->get("statuses/user_timeline",$data);
//		if($timelines->isSuccess)
			return $timelines;
	}

	public static function getHomeTimeline($opts = array()){
		$connection = self::getConnection();
//		$content = $connection->get("account/verify_credentials");

//		$data = array();

//		$data['user_id'] = $content->id;
		$timelines = $connection->get("statuses/home_timeline",$opts);

		return $timelines;
	}
	public static function getStatusById($id){
		$connection = self::getConnection();
		$opts = array("id"=>$id);
//		$content = $connection->get("account/verify_credentials");

//		$data = array();

//		$data['user_id'] = $content->id;
		$timelines = $connection->get("statuses/show",$opts);

		return $timelines;
	}

	public static function getMentions($opts = array()){
		$connection = self::getConnection();

		$mentions = $connection->get("statuses/mentions_timeline",$opts);
		return $mentions;
	}
	public static function getLists($opts = array()){
		$connection = self::getConnection();

		$lists = $connection->get("lists/list",$opts);
		return $lists;
	}
}