<?php
use Phalcon\Logger\Adapter\File as FileAdapter;

class McryptLibrary
{

	const KEY_MCRYPT = "k4m4rd464n6_4pp$";

	public static function encryptString( $text )
	{
		$key = McryptLibrary::KEY_MCRYPT;
		$crypt = new Phalcon\Crypt();
		$encrypted = $crypt->encrypt($text, $key);

		return base64_encode($encrypted);
	}

	public static function decryptString( $encrypted_string )
	{
		$key = McryptLibrary::KEY_MCRYPT;
		$crypt = new Phalcon\Crypt();
		$encrypted_string = base64_decode($encrypted_string);
		$decrypted = $crypt->decrypt($encrypted_string, $key);

		return $decrypted;
	}
}