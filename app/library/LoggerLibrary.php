<?php
use Phalcon\Logger\Adapter\File as FileAdapter;

class LoggerLibrary
{

	public static function logDebug( $var, $fileName = "server.log" )
	{
		if ( !isset($GLOBALS[ 'LOGGER_APP' ]) ) {
			$dirName = str_replace("\\", "/", dirname(__FILE__)) . "/../logs/";
			if ( !file_exists($dirName) ) {
				mkdir($dirName);
			}
			$filePath = $dirName . $fileName;
			if ( !file_exists($filePath) ) {
				touch($filePath);
			}
			$GLOBALS[ 'LOGGER_APP' ] = new FileAdapter($filePath);
		}
		$isArr = is_array($var) || ($var instanceof Traversable ? TRUE : FALSE);
		if ( $isArr || !is_string($var) ) {
			$GLOBALS[ 'LOGGER_APP' ]->log(json_encode($var));
		} else {
			$GLOBALS[ 'LOGGER_APP' ]->log((string)$var);
		}
	}

	public static function logError( $var, $fileName = "server.log" )
	{
		if ( !isset($GLOBALS[ 'LOGGER_APP' ]) ) {
			$dirName = str_replace("\\", "/", dirname(__FILE__)) . "/../logs/";
			if ( !file_exists($dirName) ) {
				mkdir($dirName);
			}
			$filePath = $dirName . $fileName;
			if ( !file_exists($filePath) ) {
				touch($filePath);
			}
			$GLOBALS[ 'LOGGER_APP' ] = new FileAdapter($filePath);
		}
		$isArr = is_array($var) || ($var instanceof Traversable ? TRUE : FALSE);
		if ( $isArr || !is_string($var) ) {
			$GLOBALS[ 'LOGGER_APP' ]->error(json_encode($var));
		} else {
			$GLOBALS[ 'LOGGER_APP' ]->error((string)$var);
		}
	}
}