<?php
use Phalcon\Logger\Adapter\File as FileAdapter;

class GCMLibrary
{
    public static $GOOGLE_API_KEY = "AIzaSyD4AcKYjGMMVdYSrPWefPdOB8D-re3J5uE";

    public static  function send_notification($registatoin_ids, $message) {
        // include config

        // Set POST variables
        $url = 'https://android.googleapis.com/gcm/send';

        $fields = array(
            'registration_ids' => $registatoin_ids,
            'data' => $message,
        );
        $fields = array(
            'registration_ids'  => $registatoin_ids,
            'data'              => array(
                "msg" => $message,
                "action"=>"VERIFICATION"
            ),
        );

        $headers = array(
            'Authorization: key=' . GCMLibrary::$GOOGLE_API_KEY,
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            //die('Curl failed: ' . curl_error($ch));
        }

        // Close connection
        curl_close($ch);
        //echo $result;
    }
}