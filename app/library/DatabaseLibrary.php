<?php
use Phalcon\Logger\Adapter\File as FileAdapter;

class DatabaseLibrary
{

	public static function importSql( $host, $user, $pass, $dbname, $sql_file, $isDropDB = FALSE )
	{
		if ( !file_exists($sql_file) ) {
			die('Input the SQL filename correctly! <button onclick="window.history.back();">Click Back</button>');
		}
		$allLines = file($sql_file);


		if ( $isDropDB ) {
			$mysqli = new mysqli($host, $user, $pass);
			$mysqli->query('DROP DATABASE IF EXISTS ' . $dbname . ";");
			$mysqli->query('CREATE DATABASE ' . $dbname . ";");
		}
		$mysqli = new mysqli($host, $user, $pass, $dbname);
		//$mysqli->query('USE '.$dbname.";");
		$zzzzzz = $mysqli->query('SET foreign_key_checks = 0');
		preg_match_all("/\nCREATE TABLE(.*?)\`(.*?)\`/si", "\n" . file_get_contents($sql_file), $target_tables);
		foreach ($target_tables[ 2 ] as $table) {
			$mysqli->query('DROP TABLE IF EXISTS ' . $table);
		}
		$zzzzzz = $mysqli->query('SET foreign_key_checks = 1');
		$mysqli->query("SET NAMES 'utf8'");
		$templine = ''; // Temporary variable, used to store current query
		foreach ($allLines as $line) { // Loop through each line
			if ( substr($line, 0, 2) != '--' && $line != '' ) { // Skip it if it's a comment
				$templine .= $line; // Add this line to the current segment
				if ( substr(trim($line), -1, 1) == ';' ) {// If it has a semicolon at the end, it's the end of the query
					$mysqli->query($templine) or print('Error performing query \'<strong>' . $templine . '\': ' . $mysqli->error . '<br /><br />');
					$templine = '';// Reset temp variable to empty
				}
			}
		}
		echo 'Importing finished.';
	}

	//EXPORT_TABLES("localhost","user","pass","db_name" );  //or add 5th parameter(array of specific tables): array("mytable1","mytable2","mytable3")

	function exportSql( $host, $user, $pass, $name, $tables = FALSE, $backup_name = FALSE )
	{
		$mysqli = new mysqli($host, $user, $pass, $name);
		$mysqli->select_db($name);
		$mysqli->query("SET NAMES 'utf8'");
		$queryTables = $mysqli->query('SHOW TABLES');
		while ($row = $queryTables->fetch_row()) {
			$target_tables[ ] = $row[ 0 ];
		}
		if ( $tables !== FALSE ) {
			$target_tables = array_intersect($target_tables, $tables);
		}

		$content = '';    //start cycle
		foreach ($target_tables as $table) {
			$result = $mysqli->query('SELECT * FROM ' . $table);
			$fields_amount = $result->field_count;
			$rows_num = $mysqli->affected_rows;
			$res = $mysqli->query('SHOW CREATE TABLE ' . $table);
			$TableMLine = $res->fetch_row();
			$content .= "\n\n" . $TableMLine[ 1 ] . ";\n\n";
			for ($i = 0; $i < $fields_amount; $i++) {
				$st_counter = 0;
				while ($row = $result->fetch_row()) {
					//when started (and every after 100 command cycle)
					if ( $st_counter % 100 == 0 || $st_counter == 0 ) {
						$content .= "\nINSERT INTO " . $table . " VALUES";
					}
					$content .= "\n(";
					for ($j = 0; $j < $fields_amount; $j++) {
						$row[ $j ] = str_replace("\n", "\\n", addslashes($row[ $j ]));
						if ( isset($row[ $j ]) ) {
							$content .= '"' . $row[ $j ] . '"';
						} else {
							$content .= '""';
						}
						if ( $j < ($fields_amount - 1) ) {
							$content .= ',';
						}
					}
					$content .= ")";
					//every after 100 command cycle [or at last line] ....p.s. but should be inserted 1 cycle eariler
					if ( (($st_counter + 1) % 100 == 0 && $st_counter != 0) || $st_counter + 1 == $rows_num ) {
						$content .= ";";
					} else {
						$content .= ",";
					}
					$st_counter = $st_counter + 1;
				}
			}
			$content .= "\n\n\n";
		}
		//save file
		$backup_name = $backup_name ? $backup_name : $name . "___(" . date('H-i-s') . "_" . date('d-m-Y') . ")__rand" . rand(1, 11111111) . ".sql";
		header('Content-Type: application/octet-stream');
		header("Content-Transfer-Encoding: Binary");
		header("Content-disposition: attachment; filename=\"" . $backup_name . "\"");
		echo $content;
		exit;
	}
}