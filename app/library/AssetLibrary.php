<?php
use Phalcon\Logger\Adapter\File as FileAdapter;

class AssetLibrary
{

	public static function get_photo_profile($path)
	{
		return isset($path)?$path:"/img/unknown-person.png";
	}

}