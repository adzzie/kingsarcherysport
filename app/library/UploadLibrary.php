<?php
use Phalcon\Logger\Adapter\File as FileAdapter;

class UploadLibrary
{

	public static function upload_picture( $file )
	{
		$isSuccess = TRUE;
		$dirName = str_replace("\\", "/", dirname(__FILE__)) . "/../..";
		$relativeDirName = "/public/uploads/pictures/" . date("Y_m_d");
		$absoluteDirName = $dirName . $relativeDirName;
		if ( !file_exists($absoluteDirName) ) {
			mkdir($absoluteDirName, 0755);
		}
		$name = $file[ 'name' ];
		$extension = pathinfo($file[ 'name' ], PATHINFO_EXTENSION);
		$uid = generateUuid();
        $fileName =  $uid. "." . $extension;
        $fileNameThumbnails = $uid. "_thumbnails." . $extension;
        $fileNameSmall = $uid. "_small." . $extension;
        $fileNameMedium = $uid. "_medium." . $extension;
        $fileNameLarge = $uid. "_large." . $extension;
		$relativePath = ($relativeDirName . "/" . $fileName);
        $relativePathThumbnails = ($relativeDirName . "/" . $fileNameThumbnails);
        $relativePathSmall = ($relativeDirName . "/" . $fileNameSmall);
        $relativePathMedium = ($relativeDirName . "/" . $fileNameMedium);
        $relativePathLarge = ($relativeDirName . "/" . $fileNameLarge);
		$path = ($absoluteDirName . "/" . $fileName);
        $pathThumbnails = ($absoluteDirName . "/" . $fileNameThumbnails);
        $pathSmall = ($absoluteDirName . "/" . $fileNameSmall);
        $pathMedium = ($absoluteDirName . "/" . $fileNameMedium);
        $pathLarge = ($absoluteDirName . "/" . $fileNameLarge);
        $isSuccess = move_uploaded_file($file[ 'tmp_name' ], $path) !== FALSE;
		if($isSuccess){
            $resize = new ResizeImageLibrary($path);
            $resize->resizeTo(ResizeImageLibrary::$thumbnail_size,ResizeImageLibrary::$thumbnail_size);
            $resize->saveImage($pathThumbnails);
            $resize->resizeTo(ResizeImageLibrary::$small_size,ResizeImageLibrary::$small_size);
            $resize->saveImage($pathSmall);
            $resize->resizeTo(ResizeImageLibrary::$medium_size,ResizeImageLibrary::$medium_size);
            $resize->saveImage($pathMedium);
            $resize->resizeTo(ResizeImageLibrary::$large_size,ResizeImageLibrary::$large_size);
            $resize->saveImage($pathLarge);
        }
        $data = array(
			"isSuccess" => $isSuccess,
			"extension" => $extension,
			"name" => $file[ 'name' ],
			"path" => $relativePath,
            "path_thumbnails" => $relativePathThumbnails,
            "path_small" => $relativePathSmall,
            "path_medium" => $relativePathMedium,
            "path_large" => $relativePathLarge
		);

		return $data;
	}

	public static function upload_picture_base64( $file )
	{
		$isSuccess = TRUE;
		$base64 = "";
		$extension = "";
		$name = "";
		$add_uri = "";
		if ( file_exists($file[ 'tmp_name' ]) ) {
			$name = $file[ 'name' ];
			$extension = pathinfo($file[ 'name' ], PATHINFO_EXTENSION);
			$contents = file_get_contents($file[ 'tmp_name' ]);
			$base64 = base64_encode($contents) !== FALSE;
			$add_uri = 'data:image/' . $extension . ';base64,';
		} else {
			$isSuccess = FALSE;
		}
		$data = array(
			"isSuccess" => $isSuccess,
			"extension" => $extension,
			"name" => $file[ 'name' ],
			"base64" => $base64,
			"add_uri" => $add_uri
		);

		return $data;
	}

    public static function upload_picture_from_base64($pic_name, $base64_str )
    {
        $isSuccess = TRUE;
        $extension = "";
        $name = "";
        $dirName = str_replace("\\", "/", dirname(__FILE__)) . "/../..";
        $relativeDirName = "/public/uploads/pictures/" . date("Y_m_d");
        $absoluteDirName = $dirName . $relativeDirName;
        \LoggerLibrary::logDebug($absoluteDirName);
        if ( !file_exists($absoluteDirName) ) {
            mkdir($absoluteDirName, 0755);
        }
        $b = explode(",",$base64_str);
        $encoded_string = count($b)==1?$b[0]:$b[1];
        $imgdata = base64_decode($encoded_string);

        $f = finfo_open();

        //mime_type
        $extension = str_replace("image/","",finfo_buffer($f, $imgdata, FILEINFO_MIME_TYPE));


        $name = $pic_name;
        $uid = generateUuid();
        //$fileName = (isset($name)?$name:generateUuid()) . "." . $mime_type;
        $fileName =  $uid. "." . $extension;
        $fileNameThumbnails = $uid. "_thumbnails." . $extension;
        $fileNameSmall = $uid. "_small." . $extension;
        $fileNameMedium = $uid. "_medium." . $extension;
        $fileNameLarge = $uid. "_large." . $extension;
        $relativePath = ($relativeDirName . "/" . $fileName);
        $relativePathThumbnails = ($relativeDirName . "/" . $fileNameThumbnails);
        $relativePathSmall = ($relativeDirName . "/" . $fileNameSmall);
        $relativePathMedium = ($relativeDirName . "/" . $fileNameMedium);
        $relativePathLarge = ($relativeDirName . "/" . $fileNameLarge);
        $path = ($absoluteDirName . "/" . $fileName);
        $pathThumbnails = ($absoluteDirName . "/" . $fileNameThumbnails);
        $pathSmall = ($absoluteDirName . "/" . $fileNameSmall);
        $pathMedium = ($absoluteDirName . "/" . $fileNameMedium);
        $pathLarge = ($absoluteDirName . "/" . $fileNameLarge);
        file_put_contents($path,$imgdata);
        $resize = new ResizeImageLibrary($path);
        $resize->resizeTo(ResizeImageLibrary::$thumbnail_size,ResizeImageLibrary::$thumbnail_size);
        $resize->saveImage($pathThumbnails);
        $resize->resizeTo(ResizeImageLibrary::$small_size,ResizeImageLibrary::$small_size);
        $resize->saveImage($pathSmall);
        $resize->resizeTo(ResizeImageLibrary::$medium_size,ResizeImageLibrary::$medium_size);
        $resize->saveImage($pathMedium);
        $resize->resizeTo(ResizeImageLibrary::$large_size,ResizeImageLibrary::$large_size);
        $resize->saveImage($pathLarge);
        $isSuccess = TRUE;
        $data = array(
            "isSuccess" => $isSuccess,
            "extension" => $extension,
            "name" => $fileName,
            "path_original"=>$relativePath,
            "path_thumbnails" => $relativePathThumbnails,
            "path_small" => $relativePathSmall,
            "path_medium" => $relativePathMedium,
            "path_large" => $relativePathLarge
        );

        return $data;
    }

	public static function upload_document( $file )
	{
		$isSuccess = TRUE;
		$dirName = str_replace("\\", "/", dirname(__FILE__)) . "/../..";
		$relativeDirName = "/public/uploads/documents/" . date("Y_m_d");
		$absoluteDirName = $dirName . $relativeDirName;
		if ( !file_exists($absoluteDirName) ) {
			mkdir($absoluteDirName, 0755);
		}
		$name = $file[ 'name' ];
		$extension = pathinfo($file[ 'name' ], PATHINFO_EXTENSION);
		$fileName = generateUuid() . "." . $extension;
		$relativePath = ($relativeDirName . "/" . $fileName);
		$path = ($absoluteDirName . "/" . $fileName);
		$isSuccess = move_uploaded_file($file[ 'tmp_name' ], $path) !== FALSE;
		$data = array(
			"isSuccess" => $isSuccess,
			"extension" => $extension,
			"name" => $file[ 'name' ],
			"path" => $relativePath
		);

		return $data;
	}
}