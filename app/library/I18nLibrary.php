<?php

/**
 * Created by PhpStorm.
 * User: bitsolution
 * Date: 4/7/14
 * Time: 4:14 PM
 */
class I18nLibrary
{

	public static function setLang( $lang )
	{
		$session = \Phalcon\DI::getDefault()->getSession();
		$session->set('Phalcon.I18n.code', $lang);
		$GLOBALS[ 'Phalcon.I18n.code' ] = $lang;
	}

	public static function resetLang()
	{
		$session = \Phalcon\DI::getDefault()->getSession();
		$session->set('Phalcon.I18n.code', NULL);
		$GLOBALS[ 'Phalcon.I18n.code' ] = NULL;
	}
    public static function getLangDefault()
    {
        $lang = I18nLibrary::getLang();
        return isset($lang) ? $lang : 'en';
    }
	public static function getLang()
	{
		$session = \Phalcon\DI::getDefault()->getSession();
		$langSession = $session->get('Phalcon.I18n.code');
		if ( isset($langSession) ) {
			$GLOBALS[ 'Phalcon.I18n.code' ] = $langSession;
		}

		return isset($GLOBALS[ 'Phalcon.I18n.code' ]) ? $GLOBALS[ 'Phalcon.I18n.code' ] : NULL;
	}

	protected static function loadPhrases()
	{
		if ( !isset($GLOBALS[ 'Phalcon.I18n.phrases' ]) ) {
			//echo "masuk sini";
			$di = Phalcon\DI::getDefault();
			$application = new \Phalcon\Mvc\Application($di);
			//echo $application->libraryDir;
			$dir = $di->get("languagesDir");
			$_phrases = array();
			$session = \Phalcon\DI::getDefault()->getSession();
			$langDef = I18nLibrary::getLang();
			$lang = $langDef == NULL ? "" : "_" . $langDef;
			$filename = $dir . "language" . $lang . ".php";
			//echo "filename:".$filename;
			//echo "A";
			if ( !file_exists($filename) ) {
				//echo "not exist:".$filename;
				//echo "B";
				$GLOBALS[ 'Phalcon.I18n.phrases' ] = array();

				return $GLOBALS[ 'Phalcon.I18n.phrases' ];
			}
			$f = file($filename);
			//echo "C";
			foreach ($f as $line) {
				if ( strpos($line, '=') !== FALSE ) {
					list($key, $value) = explode("=", $line);
					$_phrases[ $key ] = $value;
				}
				//echo "X:$key=>$value";
			}
			//echo "D";
			$GLOBALS[ 'Phalcon.I18n.phrases' ] = $_phrases;
		}

		return $GLOBALS[ 'Phalcon.I18n.phrases' ];
	}

	protected static function get( $key )
	{
		I18nLibrary::loadPhrases();

		return $GLOBALS[ 'Phalcon.I18n.phrases' ][ $key ];
	}

	public static function p_message( $code, $params = array() )
	{
		echo I18nLibrary::message($code, $params);
	}

	public static function message( $code, $params = array() )
	{
		$args = isset($params[ 'args' ]) ? $params[ 'args' ] : NULL;
		$default = isset($params[ 'default' ]) ? $params[ 'default' ] : NULL;
		//echo "A:".$code;
		//var_dump(I18nLibrary::loadPhrases());
		$text = "";
		if ( !array_key_exists($code, I18nLibrary::loadPhrases()) ) {

			$text = $default == NULL ? "" : $default;
			//echo "B".$text;
		} else {
			$text = I18nLibrary::get($code) == NULL ? $default : I18nLibrary::get($code);
			//echo "B1".$text;
		}
		if ( $args != NULL ) {
			//echo "C";
			foreach ($args as $key => $arg) {
				//echo "D";
				$text = str_replace("{" . $key . "}", $arg, $text);
			}
		}

		//echo "E";
		return trim($text);
	}

}