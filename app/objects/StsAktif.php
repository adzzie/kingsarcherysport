<?php
/**
 * Created by PhpStorm.
 * User: bitsolution
 * Date: 4/9/2015
 * Time: 4:41 PM
 */

class StsAktif
{
	//column type news
    public static $NON_AKTIF = 0;
    public static $AKTIF = 1;

    public static function getString($type){
        //$type = intval($type."");
        switch($type){
            case self::$AKTIF  : return "AKTIF";
            default : return "NON AKTIF";
        }
    }
}