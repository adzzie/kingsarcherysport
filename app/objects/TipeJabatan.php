<?php
/**
 * Created by PhpStorm.
 * User: bitsolution
 * Date: 4/9/2015
 * Time: 4:41 PM
 */

class TipeJabatan
{
	//column type news
    public static $MPU = 5;
    public static $DIRUT = 1;
    public static $BPH = 2;
    public static $MANAGER = 3;
    public static $MR = 4;
    public static $STAFF = 0;
    public static $BINAAN = 6;

    public static function getStringStatus($type){
        //$type = intval($type."");
        switch($type){
            case self::$MPU  : return "MPU";
            case self::$DIRUT  : return "DIREKTUR UTAMA";
            case self::$BPH  : return "BADAN PENGURUS HARIAN";
            case self::$MANAGER  : return "MANAGER";
            case self::$STAFF: return "STAFF";
            case self::$MR  : return "MR";
            default : return "BINAAN";
        }
    }
}