<?php
/**
 * Created by PhpStorm.
 * User: bitsolution
 * Date: 4/9/2015
 * Time: 4:41 PM
 */

class EnumBulanHijriyah
{
	//column type news
    public static $MUHARRAM = 1;
    public static $SAFAR = 2;
    public static $RABIUL_AWAL = 3;
    public static $RABIUL_AKHIR = 4;
    public static $JUMADIL_AWAL = 5;
    public static $JUMADIL_AKHIR = 6;
    public static $RAJAB = 7;
    public static $SYABAN = 8;
    public static $RAMADHAN = 9;
    public static $SYAWAL = 10;
    public static $DZULKAIDAH = 11;
    public static $DZULHIJJAH = 12;

    public static function getString($type){
        //$type = intval($type."");
        switch($type){
            case self::$SAFAR  : return "SAFAR";
            case self::$RABIUL_AWAL  : return "RABIUL_AWAL";
            case self::$RABIUL_AKHIR  : return "RABIUL_AKHIR";
            case self::$JUMADIL_AWAL  : return "JUMADIL_AWAL";
            case self::$JUMADIL_AKHIR  : return "JUMADIL_AKHIR";
            case self::$RAJAB  : return "RAJAB";
            case self::$SYABAN  : return "SYABAN";
            case self::$RAMADHAN  : return "RAMADHAN";
            case self::$SYAWAL  : return "SYAWAL";
            case self::$DZULKAIDAH  : return "DZULKAIDAH";
            case self::$DZULHIJJAH  : return "DZULHIJJAH";
            default : return "MUHARRAM";
        }
    }
}