<?php
/**
 * Created by PhpStorm.
 * User: bitsolution
 * Date: 4/9/2015
 * Time: 4:41 PM
 */

class StatusNikah
{
	//column type news
    public static $BELUM = 0;
    public static $MENIKAH = 1;
    public static function getStatus($type){
        //$type = intval($type."");
        switch($type){
            case self::$BELUM  : return "BELUM";
            default : return "MENIKAH";
        }
    }
}