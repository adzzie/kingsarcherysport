<?php
/**
 * Created by PhpStorm.
 * User: bitsolution
 * Date: 4/9/2015
 * Time: 4:41 PM
 */

class TypeCustomer
{
	//column type news
    public static $STAFF = 0;
    public static $MPU = 5;
    public static $DIRUT = 1;
    public static $BPH = 2;
    public static $MANAGER = 3;
    public static $MR = 4;

    public static $BINAAN = 50;

    public static $MEMBER_SMA03_T_KARYA = 6;
    public static $MEMBER_SMA02_T_UTAMA = 7;
    public static $MEMBER_SMA01_T_MADYA = 8;

    public static $MEMBER_SMP03_T_MUDA = 9;
    public static $MEMBER_SMP02_T_AKSARA = 10;
    public static $MEMBER_SMP01_T_MULA = 11;

    public static $MEMBER_SD01 = 111;
    public static $MEMBER_SD02 = 112;
    public static $MEMBER_SD03 = 113;
    public static $MEMBER_SD04 = 114;
    public static $MEMBER_SD05 = 115;
    public static $MEMBER_SD06 = 116;

    public static $PENGAJAR = 66;
    public static $MEMBER_MAHASISWA = 77;

    public static $MEMBER_OUTSOURCE = 88;

    public static $GUEST = 99;

    public static function getStringStatus($type){
        //$type = intval($type."");
        switch($type){
            case self::$MPU  : return "MPU";
            case self::$DIRUT  : return "DIREKTUR UTAMA";
            case self::$BPH  : return "BADAN PENGURUS HARIAN";
            case self::$MANAGER  : return "MANAGER";
            case self::$STAFF: return "STAFF SQ";

            case self::$BINAAN: return "BINAAN";

            case self::$MR  : return "MR";

            case self::$PENGAJAR  : return "PENGAJAR";
            case self::$MEMBER_MAHASISWA  : return "MAHASISWA";

            case self::$MEMBER_SMA03_T_KARYA  : return "SMU XII (TUNAS KARYA)";
            case self::$MEMBER_SMA02_T_UTAMA  : return "SMU XI (TUNAS UTAMA)";
            case self::$MEMBER_SMA01_T_MADYA  : return "SMU X (TUNAS MADYA)";

            case self::$MEMBER_SMP03_T_MUDA  : return "SMP 9 (TUNAS MULA)";
            case self::$MEMBER_SMP02_T_AKSARA  : return "SMP 8 (TUNAS AKSARA)";
            case self::$MEMBER_SMP01_T_MULA  : return "SMP 7 (TUNAS MUDA)";

            case self::$MEMBER_SD06  : return "SD 06";
            case self::$MEMBER_SD05  : return "SD 05";
            case self::$MEMBER_SD04  : return "SD 04";
            case self::$MEMBER_SD03  : return "SD 03";
            case self::$MEMBER_SD02  : return "SD 02";
            case self::$MEMBER_SD01  : return "SD 01";

            case self::$MEMBER_OUTSOURCE  : return "MEMBER OUTSOURCE";

            default : return "GUEST";
        }
    }
}