<?php

class StrJadwalDetil extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    public $id;

    /**
     *
     * @var string
     */
    public $id_jadwal;

    /**
     *
     * @var string
     */
    public $id_jadwal_shift;

    /**
     *
     * @var integer
     */
    public $betina_kuota;

    /**
     *
     * @var integer
     */
    public $betina_sisa_kuota;

    /**
     *
     * @var integer
     */
    public $jantan_kuota;

    /**
     *
     * @var integer
     */
    public $jantan_sisa_kuota;

    /**
     *
     * @var integer
     */
    public $tot_kuota;

    /**
     *
     * @var integer
     */
    public $tot_sisa;

    /**
     *
     * @var string
     */
    public $date_created;

    /**
     *
     * @var string
     */
    public $date_update;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'id_jadwal' => 'id_jadwal', 
            'id_jadwal_shift' => 'id_jadwal_shift', 
            'betina_kuota' => 'betina_kuota', 
            'betina_sisa_kuota' => 'betina_sisa_kuota', 
            'jantan_kuota' => 'jantan_kuota', 
            'jantan_sisa_kuota' => 'jantan_sisa_kuota', 
            'tot_kuota' => 'tot_kuota', 
            'tot_sisa' => 'tot_sisa', 
            'date_created' => 'date_created', 
            'date_update' => 'date_update'
        );
    }

}
