<?php

class StrDataKucing extends \Phalcon\Mvc\Model
{

    public $id;
    public $id_user;
    public $nama;

    // 0 = LOKAL, 1 = RAS
    public $jns_kucing;

    // 0 = BETINA, 1 = JANTAN
    public $jns_kelamin;
    public $jumlah;
    public $deskripsi;
    public $date_created;
    public $date_updated;
    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'id_user' => 'id_user', 
            'nama' => 'nama', 
            'jns_kucing' => 'jns_kucing', 
            'jns_kelamin' => 'jns_kelamin', 
            'jumlah' => 'jumlah', 
            'deskripsi' => 'deskripsi', 
            'date_created' => 'date_created', 
            'date_updated' => 'date_updated'
        );
    }

    public function initialize()
    {
        $this->belongsTo('id_user', 'StrUser', 'id', array("alias"=>"str_user"));
    }

}
