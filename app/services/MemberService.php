<?php

class MemberService extends ServiceBase{

	public static function loginAdmin( $username, $password ){

		$response = new ResponseObject();
		$response->isSuccess = TRUE;

		$user = \StrAccount::findFirst(array(
			"conditions"=>"username=:username: AND password=:password: ",
			"bind" => array(
				"username" => $username,
				"password" => md5($password)
			)
		));

		if ( !$user ) {
			$response->isSuccess = FALSE;
            $response->message = "Maaf, user tidak ditemukan.";
		}else{
			$response->message = "Terima Kasih.";
        }
		$response->data = $user;

		return $response;
	}

	public static function signinMemberApp($username, $password, $device_id, $os_type, $app_key){
        $response = new ResponseObject();
        $response->isSuccess = TRUE;
        $user_data = NULL;

        $user = \StrAccount::findFirst(array(
            "username=:username: AND password=:password: ",
            "bind" => array(
                "username" => $username,
                "password" => md5($password)
            )
        ));

        $user_data = object_to_array($user);
        $response->isSuccess = TRUE;
        $response->data = $user_data;

        return $response;
    }

	public static function loginMemberApp( $username, $password , $device_id, $os_type, $app_key )
	{
		$response = new ResponseObject();
		$response->isSuccess = TRUE;
		$user_data = NULL;

		$user = PgPhl::findFirst(array(
			"username=:username: AND password=:password: ",
			"bind" => array(
				"username" => $username,
				"password" => md5($password)
			)
		));
		// check app_key
		$client_app_key = PgApp::findFirst(array(
			"app_key=:app_key:",
			"bind" => array(
				"app_key" => $app_key,
			)
		));

		if ( !$client_app_key ) {
			$response->isSuccess = FALSE;
			$response->message = T::message("login.message.invalidappkey");
		} else {
			if ( !$user ) {
				$response->isSuccess = FALSE;
				$response->message = T::message("login.message.error");
			} else {
				//else {
				$response->message = T::message("login.message.userloggedin");
				// get user primary photo

				//$historyLog = MemberService::createHistoryLogin($user->id,TypeLogin::$MOBILE_ANDROID);

				// create session token
				$user_token = generateRandomString(48);
				$session_token = new PgSessionToken();
				$session_token->id = generateUuidString();
				$session_token->id_app = $client_app_key->id;
				$session_token->id_member = $user->id;
				$session_token->session_key = $user_token;
				$session_token->device_id = $device_id;
				$session_token->os_type = $os_type;
				$session_token->created_time = date('Y-m-d H:i:s');
				//$session_token->session_log = $historyLog->data->id;
				if (!$session_token->save()) {
					$response->isSuccess = FALSE;
					$response->message = T::message("login.message.errorstatus");
				}
				$user_data = object_to_array($user);
				//$user_data = array_merge(object_to_array($user), $user_photo, $user_business, array("session_key" => $user_token));
			}
		}

		$response->data = $user_data;

		return $response;
	}

	public static function checkAppKey ( $app_key )
	{
		$client_app_key = PgApp::findFirst(array(
			"app_key=:app_key:",
			"bind" => array(
				"app_key" => $app_key,
			)
		));

		if ( !$client_app_key ) {
			return false;
		} else {
			return true;
		}
	}

	public static function checkSessionToken ( $session_key )
	{
		$client_session_key = PgSessionToken::findFirst(array(
			"session_key=:session_key:",
			"bind" => array(
				"session_key" => $session_key,
			)
		));

		if ( $client_session_key != false ) {
			return true;
		} else {
			return false;
		}
	}

	public static function getUserLoggedOnBySessionToken ( $session_key )
	{
		$client_session_key = PgSessionToken::findFirst(array(
			"session_key=:session_key:",
			"bind" => array(
				"session_key" => $session_key,
			)
		));

		if ( $client_session_key != false ) {
			return $client_session_key->id_member;
		} else {
			return null;
		}
	}

	public static function logoutMemberApp( $session_key, $id_member, $app_key )
	{
		$response = new ResponseObject();
		$response->isSuccess = TRUE;
		$user_data = NULL;

		// check app_key
		$client_app_key = PgApp::findFirst(array(
			"app_key=:app_key:",
			"bind" => array(
				"app_key" => $app_key,
			)
		));

		if ( !$client_app_key ) {
			$response->isSuccess = FALSE;
			$response->message = T::message("login.message.invalidappkey");
		} else {
			// check session key
			$session_app_key = PgSessionToken::findFirst(array(
				"session_key=:session_key: AND id_member=:id_member:",
				"bind" => array(
					"session_key" => $session_key,
					"id_member" => $id_member,
				)
			));

			if ( !$session_app_key ) {
				$response->isSuccess = FALSE;
				$response->message = T::message("login.message.invalidsessionkey");
			}

			if (is_found($session_app_key)) {
				if ($session_app_key->delete() === FALSE) {
					$response->isSuccess = FALSE;
					$response->message = T::message("login.message.errorlogout");
				} else {
					//MemberService::updateHistoryLogin($session_app_key->session_log);
					$response->isSuccess = TRUE;
					$response->message = T::message("login.message.successlogout");
				}
			} else {
				$response->isSuccess = FALSE;
				$response->message = T::message("login.message.erroralreadylogout");
			}
		}

		return $response;
	}

	public static function doCreateBtqMentor($dataCustomer, $POST){
		$response = new \ResponseObject();
		try {

			$data = new \SqBtqMentor();

			$oldData = \SqBtqMentor::findFirstByIdCustomer($dataCustomer->id);
			if($oldData){
				$data->id = $oldData->id;
			}else{
				$data->id = generateUuidString();
			}

			$data->id_customer = $dataCustomer->id;

			$data->nama = $dataCustomer->first_name." ".$dataCustomer->last_name;

			$data->assign($POST);
			$data->hobi = $POST['hobi'];
			$data->pendidikan = $POST['pendidikan'];
			$data->jurusan = $POST['jurusan'];
			$data->peminatan = $POST['peminatan'];

			$data->created_date = date('Y-m-d H:i:s');
			$data->update_date = date('Y-m-d H:i:s');

			$data->save();

			$response->message = \T::message("all.label.message.success_save");
		}catch (\Exception $ex){
			$response->isSuccess = false;
			$response->message = $ex->getMessage();
		}
	}

    public static function doUpdateCustomer($dataCustomer, $POST){
        $response = new \ResponseObject();
        try {
            $oldData = \SqCustomer::findFirstByEmail($dataCustomer->email);
            if($oldData){
                $oldData->assign($POST);

                $oldData->first_name = $dataCustomer->nama;

                $oldData->tgl_lahir = \DateTimeLibrary::parseDateIOtoSQL($oldData->tgl_lahir);
                $oldData->created_date = date('Y-m-d H:i:s');
                $oldData->update_date = date('Y-m-d H:i:s');

                $oldData->save();
                $response->message = \T::message("all.label.message.success_save");
            }
        }catch (\Exception $ex){
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
    }

}