<?php

/**
 * Created by PhpStorm.
 * User: bitsolution
 * Date: 2/9/2015
 * Time: 2:46 PM
 */
class MobileService extends ServiceBase
{
    public static function getUserKey()
    {
        return "5r386x";
    }
    public static function getPassKey()
    {
        return "kamardagang";
    }
	public static function sendMobileRegistration($email,$mobile,$password)
	{
		error_reporting(E_ALL);
        $username = "";
        if(isset($email) && isset($mobile)){
            $username = $email." / ".$mobile;
        }else{
            $username = $mobile;
        }
        $message = "Your registration is successfull, your username is ".$username." and password is ".$password;
        MobileService::sendMessage($mobile,$message);
        return TRUE;
	}
    public static function sendMobileSuccessForgotPassword($email,$mobile,$password)
    {
        error_reporting(E_ALL);
        $username = "";
        if(isset($email) && isset($mobile)){
            $username = $email." / ".$mobile;
        }else{
            $username = $mobile;
        }
        $message = "Reset password is successful, your new password's account of  ".$username." is ".$password;
        MobileService::sendMessage($mobile,$message);
        return TRUE;
    }
    public static function sendMobileVerificationForgot($email,$mobile,$verification_code)
    {
        error_reporting(E_ALL);
        $username = "";
        if(isset($email) && isset($mobile)){
            $username = $email." / ".$mobile;
        }else{
            $username = $mobile;
        }
        $message = "Your verification code to reset password of ".$username." is ".$verification_code;
        MobileService::sendMessage($mobile,$message);
        return TRUE;
    }
    public static function sendMobileRegistrationNewBusiness($mobile)
    {
        error_reporting(E_ALL);
        $message = "Thank you for registration your business in Kamar Dagang, your business request being processed by Kamar Dagang.";
        MobileService::sendMessage($mobile,$message);
        return TRUE;
    }
    public static function sendMobileRegistrationNewBusinessToOwner($mobile,$email,$password,$mobile_creator,$email_creator)
    {
        error_reporting(E_ALL);
        $username = "";
        if(isset($email) && isset($mobile)){
            $username = $email." / ".$mobile;
        }else{
            $username = $mobile;
        }

        $username_creator = "";
        if(isset($email_creator) && isset($mobile_creator)){
            $username_creator = $email_creator." / ".$mobile_creator;
        }else{
            $username_creator = $mobile_creator;
        }
        $message = "Your account has been activated in kamardagang by ".$username_creator.". You can login using username ".$username." is and password is ".$password.".Thanks";
        MobileService::sendMessage($mobile,$message);
        return TRUE;
    }
    public static function sendMessage($mobile,$message)
    {
        $message = urlencode($message);
        $url = "https://reguler.zenziva.net/apps/smsapi.php?userkey=".MobileService::getUserKey()."&passkey=".MobileService::getPassKey()."&nohp=".$mobile."&pesan=".$message;
        file_get_contents($url);
        LoggerLibrary::logDebug($url);
        LoggerLibrary::logDebug($message);
    }
}