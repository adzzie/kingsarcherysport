<?php

/**
 * Created by PhpStorm.
 * User: bitsolution
 * Date: 2/9/2015
 * Time: 2:46 PM
 */
class EmailService extends ServiceBase
{
    private static $emailVerificationCodeReset = "VERIFICATION_CODE_RESET_PASSWORD";
    private static $emailRegistrationAccountDetail = "REGISTRATION_ACCOUNT_DETAIL";
    private static $emailSuccessForgotPassword = "SUCCESS_FORGOT_PASSWORD";
    private static $emailRegistrationNewBusiness = "REGISTRATION_NEW_BUSINESS";
    private static $emailRegistrationNewBusinessToOwner = "REGISTRATION_NEW_BUSINESS_TO_OWNER";

    //@deprecated
	public static function sendEmailVerification( $to = "akhmad.mib@gmail.com", $name = "Much Tri Wibowo", $token = "ssVBFzv9qHW4QjWZUcy2ftRgDIESu6UcppdMSf5662PEb73Y0gcKQaK1ghidmrg2")
	{
		error_reporting(E_ALL);
        $lang = T::getLangDefault();
        $conditions = "code=:code: and lang=:lang:";
        $bind = array("code"=>EmailService::$emailVerificationCodeReset,"lang"=>$lang);
        $emailTemplate = KdgMstEmailTemplate::findFirst(array(
             "conditions" => $conditions,
             "bind" => $bind
        ));
		$base_url = base_url();
		$verification_url = $base_url . "/verification?code=" . $token;
		$subject = $emailTemplate->subject;

		$message = $emailTemplate->content;
        $arrFindReplace = array("{{base_url}}" => base_url(),
                            "{{token}}" => $token,
                            "{{to}}" => $to,
                            "{{name}}" => $name);
        $message = strtr($message,$arrFindReplace);
        $headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		// More headers
		$headers .= 'From: <noreply@kamar-dagang.tk>' . "\r\n";
		//$headers .= 'Cc: akhmad.mib@gmail.com' . "\r\n";

		mail($to, $subject, $message, $headers);

		return TRUE;
	}

    public static function sendEmailRegistration( $to = "akhmad.mib@gmail.com", $name = "Much Tri Wibowo", $mobile, $password)
    {
        error_reporting(E_ALL);
        $lang = T::getLangDefault();
        $conditions = "code=:code: and lang=:lang:";
        $bind = array("code"=>EmailService::$emailRegistrationAccountDetail,"lang"=>$lang);
        $emailTemplate = KdgMstEmailTemplate::findFirst(array(
            "conditions" => $conditions,
            "bind" => $bind
        ));

        $base_url = base_url();
        $username = "";
        if(isset($to) && isset($mobile)){
            $username = $to." / ".$mobile;
        }else{
            $username = $to;
        }

        $verification_url = $base_url . "/verification?code=";
        $subject = $emailTemplate->subject;

        $message = $emailTemplate->content;
        $arrFindReplace = array("{{base_url}}" => base_url(),
            "{{username}}" => $username,
            "{{to}}" => $to,
            "{{name}}" => $name,
            "{{mobile}}" => $mobile,
            "{{password}}" => $password);
        $message = strtr($message,$arrFindReplace);

        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: <noreply@kamar-dagang.tk>' . "\r\n";
        //$headers .= 'Cc: akhmad.mib@gmail.com' . "\r\n";

        mail($to, $subject, $message, $headers);

        return TRUE;
    }

    public static function sendEmailSuccessForgotPassword( $to = "akhmad.mib@gmail.com", $name = "Much Tri Wibowo", $mobile, $password)
    {
        error_reporting(E_ALL);
        $lang = T::getLangDefault();
        $conditions = "code=:code: and lang=:lang:";
        $bind = array("code"=>EmailService::$emailSuccessForgotPassword,"lang"=>$lang);
        $emailTemplate = KdgMstEmailTemplate::findFirst(array(
            "conditions" => $conditions,
            "bind" => $bind
        ));
        $base_url = base_url();
        $username = "";
        if(isset($to) && isset($mobile)){
            $username = $to." / ".$mobile;
        }else{
            $username = $to;
        }

        $verification_url = $base_url . "/verification?code=";
        $subject = $emailTemplate->subject;

        $message = $emailTemplate->content;
        $arrFindReplace = array("{{base_url}}" => base_url(),
            "{{username}}" => $username,
            "{{to}}" => $to,
            "{{name}}" => $name,
            "{{mobile}}" => $mobile,
            "{{password}}" => $password);
        $message = strtr($message,$arrFindReplace);

        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: <noreply@kamar-dagang.tk>' . "\r\n";
        //$headers .= 'Cc: akhmad.mib@gmail.com' . "\r\n";

        mail($to, $subject, $message, $headers);

        return TRUE;
    }

    public static function sendEmailRegistrationNewBusiness( $to = "akhmad.mib@gmail.com", $name = "Much Tri Wibowo")
    {
        error_reporting(E_ALL);
        $lang = T::getLangDefault();
        $conditions = "code=:code: and lang=:lang:";
        $bind = array("code"=>EmailService::$emailRegistrationNewBusiness,"lang"=>$lang);
        $emailTemplate = KdgMstEmailTemplate::findFirst(array(
            "conditions" => $conditions,
            "bind" => $bind
        ));
        $base_url = base_url();
        $subject = $emailTemplate->subject;

        $message = $emailTemplate->content;
        $arrFindReplace = array("{{base_url}}" => base_url(),
            "{{to}}" => $to,
            "{{name}}" => $name);
        $message = strtr($message,$arrFindReplace);

        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: <noreply@kamar-dagang.tk>' . "\r\n";
        //$headers .= 'Cc: akhmad.mib@gmail.com' . "\r\n";

        mail($to, $subject, $message, $headers);

        return TRUE;
    }
    public static function sendEmailRegistrationNewBusinessToOwner( $to = "akhmad.mib@gmail.com", $name = "Much Tri Wibowo",$mobile,$password,$mobile_creator,$email_creator)
    {
        error_reporting(E_ALL);
        $lang = T::getLangDefault();
        $conditions = "code=:code: and lang=:lang:";
        $bind = array("code"=>EmailService::$emailRegistrationNewBusinessToOwner,"lang"=>$lang);
        $emailTemplate = KdgMstEmailTemplate::findFirst(array(
            "conditions" => $conditions,
            "bind" => $bind
        ));
        $base_url = base_url();

        $username = "";
        if(isset($to) && isset($mobile)){
            $username = $to." / ".$mobile;
        }else{
            $username = $mobile;
        }

        $username_creator = "";
        if(isset($email_creator) && isset($mobile_creator)){
            $username_creator = $email_creator." / ".$mobile_creator;
        }else{
            $username_creator = $mobile_creator;
        }

        $subject = $emailTemplate->subject;

        $message = $emailTemplate->content;
        $arrFindReplace = array("{{base_url}}" => base_url(),
            "{{username_creator}}" => $username_creator,
            "{{username}}" => $username,
            "{{to}}" => $to,
            "{{name}}" => $name,
            "{{mobile}}" => $mobile,
            "{{password}}" => $password);
        $message = strtr($message,$arrFindReplace);

        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: <noreply@kamar-dagang.tk>' . "\r\n";
        //$headers .= 'Cc: akhmad.mib@gmail.com' . "\r\n";

        mail($to, $subject, $message, $headers);

        return TRUE;
    }
}